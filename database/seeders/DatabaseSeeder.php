<?php

namespace Database\Seeders;

use Database\Seeders\seeds\CategoriesTableSeeder;
use Database\Seeders\seeds\PagesTableSeeder;
use Database\Seeders\seeds\PostsTableSeeder;
use Database\Seeders\seeds\TranslationsTableSeeder;
use Database\Seeders\seeds\UsersTableSeeder;
use Database\Seeders\seeds\VoyagerDatabaseSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
