<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// php artisan storage:link
Route::get('clear_cache', function () {
    \Artisan::call('optimize:clear');
    \Artisan::call('storage:link');

  
    dd("Cache is cleared");
});
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
